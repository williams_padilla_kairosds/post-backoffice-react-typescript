import { Link } from "react-router-dom";
import styled from "styled-components";
import { themeColors } from "../themes";

export const PostContainer = styled.main`
    background-color: whitesmoke;
    border-radius: 16px;
    box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
    max-width: 30rem;
    max-height: 15rem;
    display: flex;
    flex-direction: row;
    align-items: center;
    border-radius: 15px;
`;

export const AuthorInfo = styled.aside`
    border-radius: 15px 0 0 15px;
    display: flex;
    justify-content: space-evenly;
    height: 100%;
    align-items: center;
    flex-direction: column;
    background-color: ${themeColors.darkTextColor};
    min-width: 30%;
    box-shadow: black 1px 1px 5px;
`;

export const AvatarImg = styled.img`
    width: 5rem;
    aspect-ratio: 1;
    background-color: whitesmoke;
    border-radius: 50%;
    box-shadow: black 1px 1px 10px;
`;

export const AuthorName = styled.h2`
    margin: 0;
    text-align: center;
    font-family: "Raleway", sans-serif;
    font-size: 1.2rem;
    color: whitesmoke;
`;

export const PostContent = styled.section`
    width: 100%;
    padding: 0 1rem;
    display: flex;
    flex-direction: column;
    justify-content: start;
    padding-bottom: 1rem;
`;

export const PostTitle = styled.h1`
    font-family: "Heebo", sans-serif;
    color: ${themeColors.darkTextColor};
`;

export const PostText = styled.p`
    font-family: "Raleway", sans-serif;
    margin: 0;
    width: 100%;
`;

export const PostDetails = styled(Link)`
    align-self: flex-end;
    text-decoration: none;
    color: whitesmoke;
    font-family: "Raleway", sans-serif;
    font-size: 0.8rem;
    font-weight: 900;
    padding: 0.3rem 0.6rem;
    margin: 0.5rem;
    border-radius: 15px;
    border: 2px solid ${themeColors.darkTextColor};
    color: ${themeColors.darkTextColor};
    transition: 0.3s;

    &:hover {
        border: none;
        box-shadow: black 0px 0px 1px;
        background-image: ${themeColors.background};
        color: whitesmoke;
    }
`;
