import styled from "styled-components";

export const SelectInput = styled.select`
    border: none;
    font-family: "Raleway", sans-serif;
    background-color: whitesmoke;
    padding: 0.5rem 2rem;
    border-bottom: grey 2px solid;
`;
