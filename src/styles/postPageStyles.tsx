import styled from "styled-components";
import { MainTitle } from "./landingStyles";

interface props {
    actionType?: string;
}

export const PostPageContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    padding-top: 15rem;
`;

export const AllPostsContainer = styled.main`
    width: 70%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    height: 100vh;
`;
export const MainPostContainer = styled.div`
    display: flex;
    flex-direction: row;
    margin: 1rem;
`;
export const ButtonContainer = styled.aside`
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    margin: 0 1rem;
    max-height: 15rem;
`;

export const ActionButton = styled.button<props>`
    width: 2rem;
    aspect-ratio: 1;
    border: none;
    border-radius: 50%;
    color: ${(props) => (props.actionType == "update" ? "grey" : "red")};
    background-color: ${(props) =>
        props.actionType == "update" ? "yellow" : "whitesmoke"};
    cursor: pointer;
    &:hover {
        box-shadow: #333333 0px 0px 10px;
    }
`;

export const PostPageForm = styled.form`
    width: 30%;
    position: sticky;
    height: 100%;
    top: 5rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 1rem;
    gap: 1rem;
`;

export const FormTitle = styled(MainTitle)`
    color: whitesmoke;
`;

export const ErrorMessage = styled.p`
    font-family: "Raleway", sans-serif;
    color: whitesmoke;
`;
