import { Link } from "react-router-dom";
import styled from "styled-components";
import { themeColors } from "../themes";

export const FormContainer = styled.form`
    display: flex;
    flex-direction: column;
    gap: 0.5rem;
    background-color: whitesmoke;
    border-radius: 5px;
    padding: 2rem 4rem;
    box-shadow: #444444 1px 1px 20px;
`;

export const FormTitle = styled.h1`
    text-align: center;
    font-family: "Heebo", sans-serif;
    color: #2a2a2a;
    font-size: 2rem;
`;

export const TextInput = styled.input`
    border: none;
    font-family: "Raleway", sans-serif;
    background-color: whitesmoke;
    padding: 0.5rem 2rem;
    border-bottom: grey 2px solid;
`;

export const LabelForm = styled.label`
    font-family: "Raleway", sans-serif;
    color: #373737;
    font-size: 0.8rem;
`;

export const SubmitButton = styled.button`
    margin-top: 5rem;
    border-radius: 20px;
    border: none;
    padding: 0.5rem;
    background-color: black;
    color: whitesmoke;
    font-family: "Heebo", sans-serif;
    cursor: pointer;

    &:hover {
        transition: 0.7s;
        background-image: ${themeColors.orangeGradient};
        color: #f4f4f4;
    }
`;

export const OrSeparator = styled.p`
    text-align: center;
    font-family: "Raleway", sans-serif;
`;

export const StyledLink = styled(Link)`
    text-decoration: none;
    font-family: "Raleway", sans-serif;
    align-self: center;
    width: fit-content;
    transition: 0.7s;

    &:hover {
        border-bottom: grey 1px solid;
    }
`;
