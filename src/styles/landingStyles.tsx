import styled from "styled-components";
import { themeColors } from "../themes";

interface props {
    color?: string;
}

export const MainContainer = styled.div`
    background-color: #ffe0e0;
    backdrop-filter: blur(10px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 0.5rem;
    padding: 2rem 5rem;
    width: 50%;
    border-radius: 10px;
    box-shadow: ${themeColors.shadow};
`;

export const InlineContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    width: 100%;
`;

export const RedirectButtons = styled.button<props>`
    width: 15rem;
    height: 3rem;
    border-radius: 20px;
    background-color: ${(props) =>
        props.color == "orange" ? "transparent" : "black"};
    color: ${(props) => (props.color == "green" ? "#f4f4f4" : "black")};
    border: ${(props) =>
        props.color == "orange" ? "2px solid black" : "none"};
    font-family: "Heebo", sans-serif;
    font-size: 1rem;
    cursor: pointer;
    transition: 0.5s ease-out;

    &:hover {
        background-image: ${themeColors.orangeGradient};
        border: none;
        color: #f4f4f4;
    }
`;

export const MainTitle = styled.h1`
    font-family: "Cabin Sketch", cursive;
    color: black;
    font-size: 6vw;
    text-align: center;
`;

export const LogoImg = styled.img`
    border-radius: 50%;
    width: 7rem;
    height: 7rem;
    overflow: hidden;
    margin-top: -6rem;
    box-shadow: black 0px 1px 10px;
`;

export const Navbar = styled.nav`
    width: 100%;
    height: 5rem;
    background-color: ${themeColors.glassBackground};
    position: fixed;
    top: 0;
`;
