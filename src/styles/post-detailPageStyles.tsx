import styled from "styled-components";
import {
    AuthorInfo,
    AuthorName,
    AvatarImg,
    PostContainer,
} from "./post-uiStyles";

export const PostPageContainer = styled.section`
    width: 100%;
    display: flex;
    justify-content: space-around;
    flex-direction: column;
    align-items: center;
`;

export const PostInfo = styled(PostContainer)`
    width: 100%;
    justify-content: center;
    flex-direction: column;
    min-width: 70rem;
    max-height: 35rem;
`;

export const AvatarPostDetail = styled(AvatarImg)`
    width: 8rem;
    position: absolute;
    bottom: -4rem;
`;

export const AuthorInfoContainer = styled(AuthorInfo)`
    width: 100%;
    height: 5rem;
    border-radius: 15px 15px 0 0;
    padding: 2rem 0;
    position: relative;
    display: flex;
    justify-content: flex-start;
`;

export const AuthorNameDetail = styled(AuthorName)`
    font-size: 2rem;
`;

export const PostContentDetail = styled.main`
    width: 90%;
    padding: 3rem 0;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
`;
