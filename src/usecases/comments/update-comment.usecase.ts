import { Comment, CommentRequest } from "../../interfaces/comment.dto";
import { CommentProxyService } from "../../services/comment.proxy.service";

export class UpdateCommentUseCase {
    async execute(
        postId: string,
        commentId: string,
        content: CommentRequest
    ): Promise<Comment> {
        const service = new CommentProxyService();
        const comment = await service.updateComment(postId, commentId, content);
        return comment;
    }
}
