vi.mock("../../repositories/comment.repository", () => {
    return {
        CommentRepository: vi.fn().mockImplementation(() => {
            return {
                updateComment: vi.fn().mockImplementation(() => {
                    return {
                        id: "ba5beb2d-74d9-4f8f-8a62-73ecee2bdde1",
                        authorId: "ca5460c4-b331-49ee-9cc1-bd21e19831ee",
                        author: "DumbUser",
                        content:
                            "I'm about to change everything from now on, I'm sick of all of you",
                    };
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { CommentRequest } from "../../interfaces/comment.dto";
import { UpdateCommentUseCase } from "./update-comment.usecase";

describe("Update comment use case", () => {
    it("should return the updated comment from a post", async () => {
        const useCase = new UpdateCommentUseCase();
        const postId = "f47f3c78-daff-49b7-b243-04fac2dae644";
        const commentId = "ba5beb2d-74d9-4f8f-8a62-73ecee2bdde1";

        const commentInput: CommentRequest = {
            content:
                "I'm about to change everything from now on, I'm sick of all of you",
        };
        const comment = await useCase.execute(postId, commentId, commentInput);

        expect(comment.content).toBe(commentInput.content);
    });
});
