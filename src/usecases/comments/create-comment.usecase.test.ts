vi.mock("../../repositories/comment.repository", () => {
    return {
        CommentRepository: vi.fn().mockImplementation(() => {
            return {
                createComment: vi.fn().mockImplementation(() => {
                    return {
                        id: "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8",
                        authorId: "ca9b2753-f607-44fd-8f38-29180666059f",
                        author: "StarFlow",
                        content:
                            "This a comment content test to see if everything is working fine",
                    };
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { CommentRequest } from "../../interfaces/comment.dto";
import { CreateCommentUseCase } from "./create-comment.usecase";

describe("Create a comment use case", () => {
    it("should create a new comment over an existing post", async () => {
        const useCase = new CreateCommentUseCase();
        const commentInput: CommentRequest = {
            content:
                "This a comment content test to see if everything is working fine",
        };

        const postId = "f47f3c78-daff-49b7-b243-04fac2dae644";

        const comment = await useCase.execute(postId, commentInput);

        expect(comment.content).toBe(commentInput.content);
    });
});
