import { CommentProxyService } from "../../services/comment.proxy.service";

export class DeleteCommentUseCase {
    async execute(postId: string, commentId: string): Promise<string> {
        const service = new CommentProxyService();
        const id = await service.deleteComment(postId, commentId);
        return id;
    }
}
