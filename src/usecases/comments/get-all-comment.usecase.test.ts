vi.mock("../../repositories/comment.repository", () => {
    return {
        CommentRepository: vi.fn().mockImplementation(() => {
            return {
                getComments: vi.fn().mockImplementation(() => {
                    return [
                        {
                            id: "ba5beb2d-74d9-4f8f-8a62-73ecee2bdde1",
                            authorId: "ca5460c4-b331-49ee-9cc1-bd21e19831ee",
                            author: "DumbUser",
                            content: "ewqewqeqw",
                        },
                        {
                            id: "3ef566d7-38e7-423d-b315-088a5b13130d",
                            authorId: "17ac344a-a213-497a-a32b-8d968c6697a9",
                            author: "StarFlow",
                            content: "No era bebesis era bebesos",
                        },
                    ];
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { GetAllCommentsUseCase } from "./get-all-comment.usecase";

describe("Get all comments use case", () => {
    it("should return all comments from a single post", async () => {
        const useCase = new GetAllCommentsUseCase();
        const postId = "f47f3c78-daff-49b7-b243-04fac2dae644";
        const comments = await useCase.execute(postId);

        expect(comments).toHaveLength(2);
    });
});
