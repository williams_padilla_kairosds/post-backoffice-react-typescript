import { Comment, CommentRequest } from "../../interfaces/comment.dto";
import { CommentProxyService } from "../../services/comment.proxy.service";

export class CreateCommentUseCase {
    async execute(
        postId: string,
        commentRequest: CommentRequest
    ): Promise<Comment> {
        const service = new CommentProxyService();
        const newComment = await service.createComment(postId, commentRequest);
        return newComment;
    }
}
