import { Comment } from "../../interfaces/comment.dto";
import { CommentProxyService } from "../../services/comment.proxy.service";

export class GetAllCommentsUseCase {
    async execute(postId: string): Promise<Comment[]> {
        const service = new CommentProxyService();
        const allComments = await service.getComments(postId);
        return allComments;
    }
}
