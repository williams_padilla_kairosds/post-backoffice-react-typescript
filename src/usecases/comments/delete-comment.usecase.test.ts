vi.mock("../../repositories/comment.repository", () => {
    return {
        CommentRepository: vi.fn().mockImplementation(() => {
            return {
                deleteComment: vi.fn().mockImplementation(() => {
                    return "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8";
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { DeleteCommentUseCase } from "./delete-comment.usecase";

describe("Delete a comment use case", () => {
    it("should return the deleted comment id", async () => {
        const useCase = new DeleteCommentUseCase();

        const postId = "f47f3c78-daff-49b7-b243-04fac2dae644";
        const commentId = "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8";

        const id = await useCase.execute(postId, commentId);

        expect(id).toBe(commentId);
    });
});
