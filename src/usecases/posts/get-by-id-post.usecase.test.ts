vi.mock("../../repositories/post.repository", () => {
    return {
        PostRepository: vi.fn().mockImplementation(() => {
            return {
                getPostById: vi.fn().mockImplementation(() => {
                    return {
                        id: "eec0face-1d07-42f3-a224-ed7b4be15477",
                        author: "GodAdmin",
                        title: "Hola buenas como estan",
                        text: "No era bebesis era bebesos , hahaha me partooo sueltameeeeeeeeeeee ",
                        comments: [
                            {
                                id: "ba5beb2d-74d9-4f8f-8a62-73ecee2bdde1",
                                authorId:
                                    "ca5460c4-b331-49ee-9cc1-bd21e19831ee",
                                author: "DumbUser",
                                content: "ewqewqeqw",
                            },
                            {
                                id: "3ef566d7-38e7-423d-b315-088a5b13130d",
                                authorId:
                                    "17ac344a-a213-497a-a32b-8d968c6697a9",
                                author: "StarFlow",
                                content: "No era bebesis era bebesos",
                            },
                        ],
                    };
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { PostRepository } from "../../repositories/post.repository";
import { PostProxyService } from "../../services/post.proxy.service";
import { GetPostByIdUseCase } from "./get-by-id-post.usecase";

describe("Get a post by its id use case", () => {
    it("should return a single post that match id ", async () => {
        const useCase = new GetPostByIdUseCase(
            new PostProxyService(new PostRepository())
        );
        const postId = "eec0face-1d07-42f3-a224-ed7b4be15477";
        const post = await useCase.execute(postId);

        expect(post.id).toBe(postId);
        expect(post.comments).toHaveLength(2);
    });
});
