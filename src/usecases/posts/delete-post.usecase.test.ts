vi.mock("../../repositories/post.repository", () => {
    return {
        PostRepository: vi.fn().mockImplementation(() => {
            return {
                deletePost: vi.fn().mockImplementation(() => {
                    return "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8";
                }),
            };
        }),
    };
});

import { describe, vi } from "vitest";
import { PostRepository } from "../../repositories/post.repository";
import { PostProxyService } from "../../services/post.proxy.service";
import { DeletePostUseCase } from "./delete-post.usecase";

describe("Delete post use case ", () => {
    it("should return the deleted id ", async () => {
        const useCase = new DeletePostUseCase(
            new PostProxyService(new PostRepository())
        );
        const id = "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8";
        const response = await useCase.execute(id);

        expect(response).toBe(id);
    });
});
