vi.mock("../../repositories/post.repository", () => {
    return {
        PostRepository: vi.fn().mockImplementation(() => {
            return {
                getPosts: vi.fn().mockImplementation(() => {
                    return [
                        {
                            id: "c0bf79e7-f34c-437d-a0c8-36143d6b636e",
                            author: "StarFlow",
                            title: "Me teneis muy harto",
                            text: "Ya basta con lo de bebesis y bebesos, que no todo el mundo se enteraa Pd. Me gustan los memes de gatos",
                        },
                        {
                            id: "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8",
                            author: "StarFlow",
                            title: "never again",
                            text: "I was wondeasdasdasdasdasdasdawwdasdwqfqfqwfqwfqwfqfqwfqwfqwfqwfwq...",
                        },
                    ];
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { PostRepository } from "../../repositories/post.repository";
import { PostProxyService } from "../../services/post.proxy.service";
import { GetAllPostUseCase } from "./get-all-post.usecase";

describe("Get all private posts", () => {
    it("should return all post from author", async () => {
        const useCase = new GetAllPostUseCase(
            new PostProxyService(new PostRepository())
        );
        const posts = await useCase.execute();

        expect(posts).toHaveLength(2);
    });
});
