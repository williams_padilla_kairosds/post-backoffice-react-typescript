import { PostDTO } from "../../interfaces/post.dto";
import { PostProxyService } from "../../services/post.proxy.service";

export class CreatePostUseCase {
    constructor(private service: PostProxyService) {}

    async execute(post: Partial<PostDTO>): Promise<PostDTO> {
        return await this.service.createPost(post);
    }
}
