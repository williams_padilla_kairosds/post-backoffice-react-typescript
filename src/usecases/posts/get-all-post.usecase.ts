import { PostDTO } from "../../interfaces/post.dto";
import { PostProxyService } from "../../services/post.proxy.service";

export class GetAllPostUseCase {
    constructor(private service: PostProxyService) {}

    async execute(): Promise<PostDTO[]> {
        return await this.service.getPosts();
    }
}
