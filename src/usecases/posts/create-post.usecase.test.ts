vi.mock("../../repositories/post.repository", () => {
    return {
        PostRepository: vi.fn().mockImplementation(() => {
            return {
                createPost: vi.fn().mockImplementation(() => {
                    return {
                        id: "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8",
                        author: "StarFlow",
                        title: "Hi how are you all",
                        text: "I Was wondering about the next project we're gonna be in, I'm so excited",
                    };
                }),
            };
        }),
    };
});

import { describe, expect, it, vi } from "vitest";
import { PostRequest } from "../../interfaces/post.dto";
import { PostRepository } from "../../repositories/post.repository";
import { PostProxyService } from "../../services/post.proxy.service";
import { CreatePostUseCase } from "./create-post.usecase";

describe("Create post use case", () => {
    it("should create a new post", async () => {
        const useCase = new CreatePostUseCase(
            new PostProxyService(new PostRepository())
        );
        const postInput: PostRequest = {
            title: "Hi how are you all",
            text: "I Was wondering about the next project we're gonna be in, I'm so excited",
        };

        const response = await useCase.execute(postInput);
        expect(response.text).toBe(postInput.text);
        expect(response.title).toBe(postInput.title);
    });
});
