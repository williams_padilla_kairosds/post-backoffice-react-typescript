import { PostDTO } from "../../interfaces/post.dto";
import { PostProxyService } from "../../services/post.proxy.service";

export class GetPostByIdUseCase {
    constructor(private service: PostProxyService) {}

    async execute(postId: string): Promise<PostDTO> {
        return await this.service.getPostById(postId);
    }
}
