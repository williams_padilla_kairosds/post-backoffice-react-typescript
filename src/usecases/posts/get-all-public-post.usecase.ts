import { PostDTO } from "../../interfaces/post.dto";
import { PostProxyService } from "../../services/post.proxy.service";

export class GetAllPublicPostUseCase {
    constructor(private service: PostProxyService) {}

    async execute(): Promise<PostDTO[]> {
        return await this.service.getPublicPost();
    }
}
