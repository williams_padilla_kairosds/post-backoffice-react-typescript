vi.mock("../../repositories/post.repository", () => {
    return {
        PostRepository: vi.fn().mockImplementation(() => {
            return {
                updatePost: vi.fn().mockImplementation(() => {
                    return {
                        id: "eec0face-1d07-42f3-a224-ed7b4be15477",
                        author: "GodAdmin",
                        title: "Hola buenas como estan",
                        text: "No era bebesis era bebesos , hahaha me partooo sueltameeeeeeeeeeee ",
                    };
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { PostDTO } from "../../interfaces/post.dto";
import { PostRepository } from "../../repositories/post.repository";
import { PostProxyService } from "../../services/post.proxy.service";
import { UpdatePostUseCase } from "./update-post.usecase";

describe("Update post use case", () => {
    it("should return an updated post", async () => {
        const useCase = new UpdatePostUseCase(
            new PostProxyService(new PostRepository())
        );
        const postInput: Partial<PostDTO> = {
            id: "eec0face-1d07-42f3-a224-ed7b4be15477",
            author: "GodAdmin",
            title: "Hola buenas como estan",
            text: "No era bebesis era bebesos , hahaha me partooo sueltameeeeeeeeeeee ",
        };
        const postUpdated = await useCase.execute(postInput);

        expect(postUpdated.id).toBe(postInput.id);
    });
});
