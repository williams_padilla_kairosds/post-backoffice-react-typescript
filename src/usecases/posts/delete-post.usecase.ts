import { PostProxyService } from "../../services/post.proxy.service";

export class DeletePostUseCase {
    constructor(private service: PostProxyService) {}

    async execute(postId: string): Promise<string> {
        return await this.service.deletePost(postId);
    }
}
