import { UserDTO } from "../../interfaces/user.dto";
import { UserProxyService } from "../../services/user.proxy.service";

export class CreateUserUseCase {
    constructor(private service: UserProxyService) {}

    async execute(user: UserDTO): Promise<void> {
        await this.service.createUser(user);
    }
}
