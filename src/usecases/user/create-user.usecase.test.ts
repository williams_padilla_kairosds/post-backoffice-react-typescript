vi.mock("../../repositories/user.repository", () => {
    return {
        UserRepository: vi.fn().mockImplementation(() => {
            return {
                create: vi.fn(),
            };
        }),
    };
});

import { describe, vi } from "vitest";
import { UserRepository } from "../../repositories/user.repository";
import { UserProxyService } from "../../services/user.proxy.service";
import { CreateUserUseCase } from "./create-user.usecase";

describe("Sign up use case", () => {
    it("should create a new user in database", async () => {
        const repository = new UserRepository();
        const useCase = new CreateUserUseCase(new UserProxyService(repository));
        const newUser = {
            email: "willy-author-4@hola.com",
            password: "1234",
            role: "AUTHOR",
            nickName: "bestAuthor",
        };

        await useCase.execute(newUser);

        expect(repository.create).toHaveBeenCalledTimes(1);
    });
});
