vi.mock("../../repositories/user.repository", () => {
    return {
        UserRepository: vi.fn().mockImplementation(() => {
            return {
                getRole: vi.fn().mockImplementation(() => {
                    return {
                        id: "2cd6cecf-6ffe-4fb1-8bcf-79b3aec8dcc8",
                        role: "AUTHOR",
                    };
                }),
            };
        }),
    };
});

import { describe, expect, vi } from "vitest";
import { UserRepository } from "../../repositories/user.repository";

describe("Get logged user info", () => {
    it("should return all user data info", async () => {
        const repository = new UserRepository();
        const info = await repository.getRole();

        expect(info.role).toBe("AUTHOR");
        expect(repository.getRole).toHaveBeenCalledTimes(1);
    });
});
