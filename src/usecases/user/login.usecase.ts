import { UserDTO } from "../../interfaces/user.dto";
import { UserProxyService } from "../../services/user.proxy.service";

export class LoginUseCase {
    constructor(private service: UserProxyService) {}

    async execute(loginData: UserDTO): Promise<void> {
        await this.service.login(loginData);
    }
}
