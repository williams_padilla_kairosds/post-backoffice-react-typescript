vi.mock("../../repositories/user.repository", () => {
    return {
        UserRepository: vi.fn().mockImplementation(() => {
            return {
                login: vi.fn().mockImplementation(() => {
                    return "token-string";
                }),
            };
        }),
    };
});

import { describe, vi } from "vitest";
import { UserRepository } from "../../repositories/user.repository";
import { UserProxyService } from "../../services/user.proxy.service";
import { LoginUseCase } from "./login.usecase";

describe("Sign in use case", () => {
    it("should return a token once logged", async () => {
        const repository = new UserRepository();
        const useCase = new LoginUseCase(new UserProxyService(repository));
        const user = {
            email: "willy-author-4@hola.com",
            password: "1234",
        };

        await useCase.execute(user);

        expect(repository.login).toHaveBeenCalledTimes(1);
    });
});
