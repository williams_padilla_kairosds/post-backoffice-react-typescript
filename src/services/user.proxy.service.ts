import { UserDTO } from "../interfaces/user.dto";
import { UserRepository } from "../repositories/user.repository";

export class UserProxyService {
    constructor(private repository: UserRepository) {}

    async createUser(user: UserDTO): Promise<void> {
        await this.repository.create(user);
    }

    async login(loginData: UserDTO): Promise<string> {
        const token = await this.repository.login(loginData);
        localStorage.setItem("token", token);
        return token;
    }
}
