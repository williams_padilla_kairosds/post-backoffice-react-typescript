import axios from "axios";
import { Config } from "./config.interface";

export class ConfigProxyService {
    async getConfig(): Promise<Config> {
        const response = await axios.get<Config>("/blog/config/config.json");
        return response.data;
    }
}
