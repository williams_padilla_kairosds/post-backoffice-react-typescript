import { Config } from "./config.interface";
import { ConfigProxyService } from "./config.proxy.service";

let api: string = "";
export class ConfigService {
    get config() {
        return api;
    }

    async load() {
        const service = new ConfigProxyService();
        const config: Config = await service.getConfig();
        api = config.api;
    }
}
const serviceInstanceSingleton = new ConfigService();
Object.freeze(serviceInstanceSingleton);
export default serviceInstanceSingleton;
