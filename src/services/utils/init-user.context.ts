export const initUserContext = () => {
    const userData = localStorage.getItem("userData");

    if (userData) {
        localStorage.removeItem("userData");
        return JSON.parse(userData);
    }
    return {
        id: "",
        role: "",
    };
};
