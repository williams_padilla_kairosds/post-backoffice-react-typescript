import axios, { AxiosRequestConfig } from "axios";

export const init = () => {
    axios.interceptors.request.use(function (axiosConfig: AxiosRequestConfig) {
        const token: string | null = localStorage.getItem("token");
        axiosConfig.headers!["Authorization"] = token ? `Bearer ${token}` : "";
        return axiosConfig;
    });
};
