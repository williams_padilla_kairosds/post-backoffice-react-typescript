export const initPostsContext = () => {
    const posts = localStorage.getItem("posts");

    if (posts) {
        localStorage.removeItem("posts");
        return JSON.parse(posts);
    }
    return [];
};
