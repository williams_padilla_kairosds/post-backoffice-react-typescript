import { describe, expect } from "vitest";
import { initUserContext } from "./init-user.context";

describe("User initialization context", () => {
    const localStorageMock = (() => {
        let store: any = {};
        return {
            getItem(key: string) {
                return store[key];
            },
            setItem(key: any, value: any) {
                store[key] = value.toString();
            },
            clear() {
                store = {};
            },
            removeItem(key: string) {
                delete store[key];
            },
        };
    })();
    Object.defineProperty(window, "localStorage", {
        value: localStorageMock,
    });

    beforeEach(() => {
        window.localStorage.clear();
    });
    it("should return empty values as default", () => {
        const setLocalStorage = (key: string, data: any) => {
            window.localStorage.setItem(key, JSON.stringify(data));
        };

        setLocalStorage("userData", { id: "hi", role: "" });

        const initUser = initUserContext();

        expect(initUser.id).toBe("hi");
    });

    it("should return empty values as default", () => {
        const initUser = initUserContext();
        expect(initUser.id).toBe("");
    });
});
