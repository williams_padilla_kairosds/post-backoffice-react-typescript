import { Comment, CommentRequest } from "../interfaces/comment.dto";
import { CommentRepository } from "../repositories/comment.repository";

export class CommentProxyService {
    async createComment(
        postId: string,
        comment: CommentRequest
    ): Promise<Comment> {
        const repository = new CommentRepository();
        const newComment = await repository.createComment(postId, comment);
        return newComment;
    }

    async getComments(postId: string): Promise<Comment[]> {
        const repository = new CommentRepository();
        const allComments = await repository.getComments(postId);
        return allComments;
    }

    async deleteComment(postId: string, commentId: string): Promise<string> {
        const repository = new CommentRepository();
        const id = await repository.deleteComment(postId, commentId);
        return id;
    }

    async updateComment(
        postId: string,
        commentId: string,
        content: CommentRequest
    ): Promise<Comment> {
        const repository = new CommentRepository();
        const comment = await repository.updateComment(
            postId,
            commentId,
            content
        );
        return comment;
    }
}
