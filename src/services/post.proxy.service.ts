import { PostDTO } from "../interfaces/post.dto";
import { PostRepository } from "../repositories/post.repository";

export class PostProxyService {
    constructor(private repository: PostRepository) {}

    async getPosts(): Promise<PostDTO[]> {
        return await this.repository.getPosts();
    }

    async getPostById(postId: string): Promise<PostDTO> {
        return await this.repository.getPostById(postId);
    }

    async getPublicPost(): Promise<PostDTO[]> {
        return await this.repository.getPublicPosts();
    }

    async createPost(post: Partial<PostDTO>): Promise<PostDTO> {
        return await this.repository.createPost(post);
    }

    async updatePost(post: Partial<PostDTO>): Promise<PostDTO> {
        return await this.repository.updatePost(post);
    }

    async deletePost(postId: string): Promise<string> {
        return await this.repository.deletePost(postId);
    }
}
