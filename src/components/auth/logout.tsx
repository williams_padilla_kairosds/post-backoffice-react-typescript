import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { RoleContext } from "../../store/role.context";

export const Logout = () => {
    const navigate = useNavigate();
    const userContext = useContext(RoleContext);

    const deleteToken = (): void => {
        localStorage.clear();
        userContext?.setData({ id: "", role: "" });
        navigate("/");
    };

    return (
        <>
            {localStorage.getItem("token") && (
                <button onClick={() => deleteToken()}>Logout</button>
            )}
        </>
    );
};
