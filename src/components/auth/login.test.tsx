import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { describe, it } from "vitest";
import { Login } from "./login";

describe("Login component", () => {
    it("should render form container component", () => {
        const component = render(<Login />, { wrapper: MemoryRouter });
        const email = component.getByPlaceholderText(/Type your email/i);
        const password = component.getByPlaceholderText(/Type your password/i);

        const loginButton = component.getByText(/LOGIN/);

        fireEvent.change(email, { target: { value: "test-author@hola.com" } });
        fireEvent.change(password, { target: { value: "1234" } });
        fireEvent.click(loginButton);
    });
});
