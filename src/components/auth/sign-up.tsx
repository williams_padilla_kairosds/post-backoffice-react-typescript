import React, { useState } from "react";
import { UserDTO } from "../../interfaces/user.dto";
import { UserRepository } from "../../repositories/user.repository";
import { UserProxyService } from "../../services/user.proxy.service";
import {
    FormContainer,
    FormTitle,
    LabelForm,
    OrSeparator,
    StyledLink,
    SubmitButton,
    TextInput,
} from "../../styles/loginPageStyles";
import { SelectInput } from "../../styles/signUpPageStyles";
import { CreateUserUseCase } from "../../usecases/user/create-user.usecase";

export const SignUp = () => {
    const [user, setUser] = useState<UserDTO>({
        email: "",
        password: "",
        role: "",
        nickName: "",
    });

    const signUp = async (newUser: UserDTO): Promise<void> => {
        const useCase = new CreateUserUseCase(
            new UserProxyService(new UserRepository())
        );
        await useCase.execute(newUser);
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        console.log(user);
        signUp(user);
    };

    const handleChange = (
        event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
    ) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value,
        });
    };

    return (
        <FormContainer onSubmit={handleSubmit}>
            <FormTitle>Sign Up</FormTitle>
            <LabelForm>Email</LabelForm>
            <TextInput
                placeholder="Type you email"
                name="email"
                type="text"
                onChange={handleChange}
                required
            />
            <LabelForm>Password</LabelForm>
            <TextInput
                placeholder="Type your password"
                type="password"
                onChange={handleChange}
                name="password"
                required
            />
            <LabelForm>Role</LabelForm>
            <SelectInput required name="role" onChange={handleChange}>
                <option value="">Select a role</option>
                <option value="AUTHOR">Author</option>
                <option value="USER">User</option>
            </SelectInput>
            <LabelForm>Nickname</LabelForm>
            <TextInput
                placeholder="Type your nickname"
                name="nickName"
                type="text"
                onChange={handleChange}
                required
            />
            <SubmitButton type="submit">Register</SubmitButton>
            <OrSeparator>or</OrSeparator>
            <StyledLink to="/login">Login</StyledLink>
        </FormContainer>
    );
};
