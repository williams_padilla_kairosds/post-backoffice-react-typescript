import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserDTO } from "../../interfaces/user.dto";
import { UserRepository } from "../../repositories/user.repository";
import { UserProxyService } from "../../services/user.proxy.service";
import { RoleContext } from "../../store/role.context";
import {
    FormContainer,
    FormTitle,
    LabelForm,
    OrSeparator,
    StyledLink,
    SubmitButton,
    TextInput,
} from "../../styles/loginPageStyles";
import { LoginUseCase } from "../../usecases/user/login.usecase";

export const Login = () => {
    const context = useContext(RoleContext);
    const [user, setUser] = useState<UserDTO>({
        email: "",
        password: "",
    });

    const navigate = useNavigate();

    const login = async (loginData: UserDTO): Promise<void> => {
        const userCase = new LoginUseCase(
            new UserProxyService(new UserRepository())
        );
        await userCase.execute(loginData);

        const repository = new UserRepository();
        const data = await repository.getRole();
        context?.setData(data);
        localStorage.getItem("token") && navigate("/home");
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        login(user);
    };

    return (
        <FormContainer onSubmit={handleSubmit}>
            <FormTitle>Login</FormTitle>
            <LabelForm>Email</LabelForm>
            <TextInput
                placeholder="Type your email"
                type="text"
                onChange={handleChange}
                name="email"
            />
            <LabelForm>Password</LabelForm>
            <TextInput
                placeholder="Type your password"
                type="password"
                onChange={handleChange}
                name="password"
            />
            <SubmitButton type="submit">LOGIN</SubmitButton>
            <OrSeparator>or</OrSeparator>
            <StyledLink to="/sign-up">Sign up</StyledLink>
        </FormContainer>
    );
};
