import { Navbar } from "../styles/landingStyles";
import { Logout } from "./auth/logout";

export const NavBar = () => {
    return (
        <Navbar>
            <Logout />
        </Navbar>
    );
};
