import { useContext, useEffect } from "react";
import { Comment } from "../interfaces/comment.dto";
import { RoleContext } from "../store/role.context";

interface CommentInput {
    comments: Comment[];
    deleteComment: deleteComment;
    postId: string | undefined;
    update: (comment: Comment) => void;
}

interface deleteComment {
    (postId: string | undefined, commentId: string | undefined): Promise<void>;
}

export const Comments = ({
    comments,
    update,
    postId,
    deleteComment: deleteComment,
}: CommentInput) => {
    const userContext = useContext(RoleContext);

    // console.log("user logeado", userContext?.data);

    const allowButtons = (authorId: string | undefined) => {
        if (userContext?.data.role !== "ADMIN") {
            if (userContext?.data.id !== authorId) return false;
        }
        return true;
    };

    const load = () => {
        localStorage.setItem("userData", JSON.stringify(userContext!.data));
    };

    useEffect(() => {
        window.addEventListener("beforeunload", load);
        return () => window.removeEventListener("beforeunload", load);
    }, []);

    return (
        <div>
            {comments?.map((comment) => {
                return (
                    <span key={comment.id}>
                        <p>Created By: {comment.author}</p>
                        {comment.content}
                        {allowButtons(comment.authorId) ? (
                            <>
                                <button
                                    onClick={() =>
                                        deleteComment(postId, comment.id)
                                    }
                                >
                                    Delete
                                </button>
                                <button onClick={() => update(comment)}>
                                    update
                                </button>
                            </>
                        ) : (
                            ""
                        )}
                    </span>
                );
            })}
        </div>
    );
};
