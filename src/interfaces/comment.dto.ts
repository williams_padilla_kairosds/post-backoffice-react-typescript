export interface Comment {
    id?: string;
    authorId?: string;
    author?: string;
    content: string;
}

export interface CommentRequest {
    content: string;
}
