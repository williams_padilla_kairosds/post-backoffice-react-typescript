export interface UserDTO {
    email: string;
    password: string;
    role?: string;
    nickName?: string;
}
