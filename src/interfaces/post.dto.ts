import { Comment } from "./comment.dto";

export interface PostDTO {
    id: string;
    author?: string;
    title: string | undefined;
    text: string | undefined;
    comments?: Comment[];
}

export interface PostRequest {
    title: string;
    text: string;
}
