import { useNavigate } from "react-router-dom";
import { RedirectButtons } from "../styles/landingStyles";

interface props {
    path: string;
    name: string;
    color?: string;
}

export const ButtonLink = ({ path, name, color }: props) => {
    const navigate = useNavigate();

    const redirect = (): void => {
        navigate(path);
    };

    return (
        <RedirectButtons color={color} onClick={redirect}>
            {name}
        </RedirectButtons>
    );
};
