import { PostDTO } from "../interfaces/post.dto";
import {
    AuthorInfo,
    AuthorName,
    AvatarImg,
    PostContainer,
    PostContent,
    PostDetails,
    PostText,
    PostTitle,
} from "../styles/post-uiStyles";

interface props {
    post: PostDTO;
}

export const Post = ({ post }: props) => {
    return (
        <PostContainer>
            <AuthorInfo>
                <AvatarImg
                    src={`https://avatars.dicebear.com/api/big-smile/${
                        post.author
                    }${".svg"}`}
                    alt="Author avatar"
                />
                <AuthorName> {post.author}</AuthorName>
            </AuthorInfo>
            <PostContent>
                <PostTitle>{post.title}</PostTitle>
                <PostText>{post.text}</PostText>
                <PostDetails to={`/post/${post.id}`}>Comments</PostDetails>
            </PostContent>
        </PostContainer>
    );
};
