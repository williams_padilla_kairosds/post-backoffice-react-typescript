export const themeColors = {
    background: "linear-gradient(110deg, #21D4FD 0%, #B721FF 100%);",
    shadow: "rgba(0, 0, 0, 0.1) 0px 4px 12px;",
    glassBackground: " rgba(255, 255, 255, 0.15);",
    greenGradient: "linear-gradient(to right, #43e97b 0%, #38f9d7 100%)",
    orangeGradient: "linear-gradient(to right, #fa709a 0%, #fee140 100%);",
    darkTextColor: "#31354e",
};
