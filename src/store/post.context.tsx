import { createContext, SetStateAction, useState } from "react";
import { PostDTO } from "../interfaces/post.dto";
import { initPostsContext } from "../services/utils/init-posts.context";

export interface posts {
    allPost: PostDTO[];
    setAllPost: React.Dispatch<SetStateAction<PostDTO[]>>;
}

export const PostContext = createContext<posts | null>(null);
PostContext.displayName = "Post";

export const PostProvider = ({ children }: any) => {
    const [allPost, setAllPost] = useState<PostDTO[]>(() => initPostsContext());

    return (
        <PostContext.Provider value={{ allPost, setAllPost }}>
            {children}
        </PostContext.Provider>
    );
};
