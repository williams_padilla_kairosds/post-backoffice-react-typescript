import { createContext, SetStateAction, useState } from "react";
import { initUserContext } from "../services/utils/init-user.context";

export interface data {
    data: UserInfo;
    setData: React.Dispatch<SetStateAction<UserInfo>>;
}

export interface UserInfo {
    id: string;
    role: string;
}

export const RoleContext = createContext<data | null>(null);
RoleContext.displayName = "role";

export const RoleProvider = ({ children }: any) => {
    const [data, setData] = useState(() => initUserContext());

    return (
        <RoleContext.Provider value={{ data, setData }}>
            {children}
        </RoleContext.Provider>
    );
};
