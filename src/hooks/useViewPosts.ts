import { useContext, useEffect } from "react";
import { PostRepository } from "../repositories/post.repository";
import { PostProxyService } from "../services/post.proxy.service";
import { PostContext, posts } from "../store/post.context";
import { GetAllPublicPostUseCase } from "../usecases/posts/get-all-public-post.usecase";

export const useViewPosts = () => {
    const posts: posts | null = useContext(PostContext);

    const getPosts = async () => {
        const publicPostUseCase = new GetAllPublicPostUseCase(
            new PostProxyService(new PostRepository())
        );

        const response = await publicPostUseCase.execute();
        posts?.setAllPost(response);
    };

    useEffect(() => {
        getPosts();
    }, [localStorage.getItem("token")]);

    return { posts };
};
