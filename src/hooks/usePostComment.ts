import { useEffect, useState } from "react";
import { Comment, CommentRequest } from "../interfaces/comment.dto";
import { PostDTO } from "../interfaces/post.dto";
import { Type } from "../pages/posts";
import { PostRepository } from "../repositories/post.repository";
import { PostProxyService } from "../services/post.proxy.service";
import { CreateCommentUseCase } from "../usecases/comments/create-comment.usecase";
import { DeleteCommentUseCase } from "../usecases/comments/delete-comment.usecase";
import { UpdateCommentUseCase } from "../usecases/comments/update-comment.usecase";
import { GetPostByIdUseCase } from "../usecases/posts/get-by-id-post.usecase";

export const usePostComment = (postId: string | undefined) => {
    const [post, setPost] = useState<PostDTO>({
        id: "",
        author: "",
        title: "",
        text: "",
    });
    const [comments, setComments] = useState<Comment[]>([]);
    const [inputValues, setInputValues] = useState<Comment>({
        content: "",
    });
    const [submitType, setSubmitType] = useState<Type>(Type.CREATE);

    const getPostInfo = async (postId: string | undefined) => {
        if (postId !== undefined) {
            const useCase = new GetPostByIdUseCase(
                new PostProxyService(new PostRepository())
            );
            const paramPost = await useCase.execute(postId);
            setPost(paramPost);
            paramPost.comments && setComments(paramPost.comments);
        }
        return;
    };

    const deleteComment = async (
        postId: string | undefined,
        commentId: string | undefined
    ): Promise<void> => {
        if (postId !== undefined && commentId !== undefined) {
            const useCase = new DeleteCommentUseCase();
            const id = await useCase.execute(postId, commentId);
            setComments(
                comments.filter((comment) => {
                    comment.id !== id;
                })
            );
            getPostInfo(postId);
        }
        return;
    };

    const createComment = async (
        postId: string | undefined,
        content: CommentRequest
    ) => {
        if (postId !== undefined) {
            const useCase = new CreateCommentUseCase();
            const newComment = await useCase.execute(postId, content);
            setComments([...comments, newComment]);
        }
        return;
    };

    const updateComment = async (
        postId: string | undefined,
        commentId: string | undefined,
        content: Comment | undefined
    ): Promise<void> => {
        if (
            commentId !== undefined &&
            postId !== undefined &&
            content !== undefined
        ) {
            const useCase = new UpdateCommentUseCase();
            const updatedComment = await useCase.execute(
                postId,
                commentId,
                content
            );
            setComments(
                comments.map((comment) => {
                    return comment.id == updatedComment.id
                        ? updatedComment
                        : comment;
                })
            );
            handleClear();
        }
        return;
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;

        setInputValues({
            ...inputValues,
            [name]: value,
        });
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        submitType == Type.CREATE
            ? createComment(postId, inputValues)
            : updateComment(postId, inputValues.id, inputValues);
        handleClear();
    };

    const handleClear = () => {
        setInputValues({ content: "" });
        setSubmitType(Type.CREATE);
    };

    const handleUpdate = (comment: Comment) => {
        setSubmitType(Type.UPDATE);
        setInputValues(comment);
    };

    useEffect(() => {
        console.log("Rendering Post Detail Page");
        getPostInfo(postId);
    }, [localStorage.getItem("token")]);

    return {
        post,
        comments,
        submitType,
        inputValues,
        createComment,
        deleteComment,
        handleChange,
        handleSubmit,
        handleUpdate,
    };
};
