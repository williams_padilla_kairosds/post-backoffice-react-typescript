import { useContext, useEffect, useState } from "react";
import { error } from "../interfaces/error.dto";
import { PostDTO } from "../interfaces/post.dto";
import { Type } from "../pages/posts";
import { PostRepository } from "../repositories/post.repository";
import { PostProxyService } from "../services/post.proxy.service";
import { PostContext, posts } from "../store/post.context";
import { RoleContext } from "../store/role.context";
import { CreatePostUseCase } from "../usecases/posts/create-post.usecase";
import { DeletePostUseCase } from "../usecases/posts/delete-post.usecase";
import { GetAllPostUseCase } from "../usecases/posts/get-all-post.usecase";
import { GetAllPublicPostUseCase } from "../usecases/posts/get-all-public-post.usecase";
import { UpdatePostUseCase } from "../usecases/posts/update-post.usecase";

const INITIAL_VALUES = {
    title: "",
    text: "",
};

export const usePost = () => {
    const posts: posts | null = useContext(PostContext);
    const userContext = useContext(RoleContext);

    const [submitType, setSubmitType] = useState<Type>(Type.CREATE);
    const [inputValues, setInputValues] =
        useState<Partial<PostDTO>>(INITIAL_VALUES);
    const [errors, setErrors] = useState<error>(INITIAL_VALUES);

    const validate = (name: string, value: string) => {
        switch (name) {
            case "title":
                if (value.length < 5 || value.length > 30) {
                    setErrors({
                        ...errors,
                        title: "Title length must be between 5 and 30 characters",
                    });
                } else {
                    setErrors(INITIAL_VALUES);
                }
                break;
            case "text":
                if (value.length < 50 || value.length > 300) {
                    setErrors({
                        ...errors,
                        text: "Text length must be between 50 and 300",
                    });
                } else {
                    setErrors(INITIAL_VALUES);
                }
                break;
        }
    };

    const getPublicPosts = async () => {
        const publicPostUseCase = new GetAllPublicPostUseCase(
            new PostProxyService(new PostRepository())
        );

        const response = await publicPostUseCase.execute();
        posts?.setAllPost(response);
    };

    const getPrivatePosts = async () => {
        const useCase = new GetAllPostUseCase(
            new PostProxyService(new PostRepository())
        );
        const response = await useCase.execute();
        posts?.setAllPost(response);
    };

    const createPost = async (post: Partial<PostDTO>) => {
        const useCase = new CreatePostUseCase(
            new PostProxyService(new PostRepository())
        );
        const postCreated = await useCase.execute(post);
        posts?.setAllPost([...posts.allPost, postCreated]);
    };

    const deletePost = async (postId: string): Promise<void> => {
        const useCase = new DeletePostUseCase(
            new PostProxyService(new PostRepository())
        );
        const deletedPostId = await useCase.execute(postId);
        posts?.setAllPost((posts) => {
            return posts.filter((prevPost) => {
                prevPost.id != deletedPostId;
            });
        });

        userContext?.data.role == "ADMIN"
            ? getPublicPosts()
            : getPrivatePosts();
    };

    const updatePost = async (post: Partial<PostDTO>) => {
        const useCase = new UpdatePostUseCase(
            new PostProxyService(new PostRepository())
        );
        const postUpdated = await useCase.execute(post);
        posts?.setAllPost((posts: PostDTO[]) => {
            return posts.map((previousPosts: PostDTO) => {
                return previousPosts.id == postUpdated.id
                    ? postUpdated
                    : previousPosts;
            });
        });
    };

    const handleChange = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { name, value } = event.target;
        validate(name, value);
        setInputValues({
            ...inputValues,
            [name]: value,
        });
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (
            Object.keys(inputValues).length !== 0 &&
            inputValues.text !== "" &&
            inputValues.title !== ""
        ) {
            submitType == Type.CREATE
                ? createPost(inputValues)
                : updatePost(inputValues);
        } else {
            alert("Errors detected");
            setErrors(INITIAL_VALUES);
        }
        handleClear();
    };

    const handleUpdate = (post: Partial<PostDTO>) => {
        setSubmitType(Type.UPDATE);
        setInputValues(post);
    };

    const handleClear = () => {
        setInputValues(INITIAL_VALUES);
        setSubmitType(Type.CREATE);
    };

    const load = () => {
        localStorage.setItem("posts", JSON.stringify(posts!.allPost));
        localStorage.setItem("userData", JSON.stringify(userContext!.data));
    };

    useEffect(() => {
        userContext?.data.role == "ADMIN"
            ? getPublicPosts()
            : getPrivatePosts();
        window.addEventListener("beforeunload", load);
        return () => window.removeEventListener("beforeunload", load);
    }, [localStorage.getItem("token")]);

    return {
        posts,
        inputValues,
        submitType,
        errors,
        handleChange,
        handleUpdate,
        handleClear,
        handleSubmit,
        deletePost,
    };
};
