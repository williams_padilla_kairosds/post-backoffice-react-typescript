import axios from "axios";
import { UserDTO } from "../interfaces/user.dto";
import serviceInstanceSingleton from "../services/config/config.service";
import { UserInfo } from "../store/role.context";

const env = serviceInstanceSingleton;

export class UserRepository {
    async create(user: UserDTO): Promise<void> {
        await axios.post(`${env.config}sign-up/`, user);
    }

    async login(loginData: UserDTO): Promise<string> {
        const response: any = await axios.post(
            `${env.config}login/`,
            loginData
        );
        return response.data.token;
    }

    async getRole(): Promise<UserInfo> {
        const response = await axios.get(`${env.config}auth/role/me`);
        return response.data;
    }
}
