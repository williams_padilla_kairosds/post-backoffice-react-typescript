import axios from "axios";
import { PostDTO } from "../interfaces/post.dto";
import serviceInstanceSingleton from "../services/config/config.service";

const env = serviceInstanceSingleton;

export class PostRepository {
    async getPosts(): Promise<PostDTO[]> {
        const response = await axios.get(`${env.config}post`);
        return response.data;
    }

    async getPublicPosts(): Promise<PostDTO[]> {
        const response = await axios.get(`${env.config}post/public`);
        return response.data;
    }

    async getPostById(postId: string): Promise<PostDTO> {
        const response = await axios.get(`${env.config}post/${postId}`);
        return response.data;
    }

    async createPost(post: Partial<PostDTO>): Promise<PostDTO> {
        const response = await axios.post(`${env.config}post`, post);
        return response.data;
    }

    async updatePost(post: Partial<PostDTO>): Promise<PostDTO> {
        const response = await axios.patch(
            `${env.config}post/${post.id}`,
            post
        );
        return response.data;
    }

    async deletePost(postId: string): Promise<string> {
        const response = await axios.delete(`${env.config}post/${postId}`);
        return response.data.id;
    }
}
