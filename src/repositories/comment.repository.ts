import axios from "axios";
import { Comment, CommentRequest } from "../interfaces/comment.dto";
import serviceInstanceSingleton from "../services/config/config.service";

const env = serviceInstanceSingleton;

export class CommentRepository {
    async createComment(
        postId: string,
        comment: CommentRequest
    ): Promise<Comment> {
        const response = await axios.post(
            `${env.config}post/${postId}/comments`,
            comment
        );
        return response.data;
    }

    async getComments(postId: string): Promise<Comment[]> {
        const response = await axios.get(
            `${env.config}post/${postId}/comments`
        );
        return response.data;
    }

    async deleteComment(postId: string, commentId: string): Promise<string> {
        const response = await axios.delete(
            `${env.config}post/${postId}/comments/${commentId}`
        );
        return response.data.id;
    }

    async updateComment(
        postId: string,
        commentId: string,
        content: CommentRequest
    ): Promise<Comment> {
        const response = await axios.patch(
            `${env.config}post/${postId}/comments/${commentId}`,
            content
        );
        return response.data;
    }
}
