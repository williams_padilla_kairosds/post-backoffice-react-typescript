import { Route, Routes } from "react-router-dom";
import { Login } from "../components/auth/login";
import { SignUp } from "../components/auth/sign-up";
import { Home } from "../pages/home";
import { Landing } from "../pages/landing";
import { PostDetail } from "../pages/post-detail";
import { Posts } from "../pages/posts";
import { PublicPosts } from "../pages/public-post";
import { PostProvider } from "../store/post.context";
import { RoleProvider } from "../store/role.context";

export const Router = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<Landing />} />
                <Route path="/sign-up" element={<SignUp />} />
            </Routes>
            <PostProvider>
                <RoleProvider>
                    <Routes>
                        <Route path="/login" element={<Login />} />
                        <Route path="/home" element={<Home />} />
                        <Route path="/post" element={<Posts />} />
                        <Route path="/post/:postId" element={<PostDetail />} />
                        <Route path="/public-posts" element={<PublicPosts />} />
                        <Route
                            path="/public-posts/:postId"
                            element={<PostDetail />}
                        />
                    </Routes>
                </RoleProvider>
            </PostProvider>
        </>
    );
};
