import { useParams } from "react-router-dom";
import { Comments } from "../components/comments";
import { usePostComment } from "../hooks/usePostComment";
import {
    AuthorInfoContainer,
    AuthorNameDetail,
    AvatarPostDetail,
    PostContentDetail,
    PostInfo,
    PostPageContainer,
} from "../styles/post-detailPageStyles";
import { PostText, PostTitle } from "../styles/post-uiStyles";

export const PostDetail = () => {
    const { postId } = useParams();

    const {
        post,
        comments,
        submitType,
        inputValues,
        deleteComment,
        handleChange,
        handleSubmit,
        handleUpdate,
    } = usePostComment(postId);

    return (
        <PostPageContainer>
            <PostInfo>
                <AuthorInfoContainer>
                    <AuthorNameDetail> {post?.author}</AuthorNameDetail>
                    <AvatarPostDetail
                        src={`https://avatars.dicebear.com/api/big-smile/${
                            post.author
                        }${".svg"}`}
                        alt="Author avatar"
                    />
                </AuthorInfoContainer>
                <PostContentDetail>
                    <PostTitle>{post?.title}</PostTitle>
                    <PostText>{post?.text}</PostText>
                </PostContentDetail>
            </PostInfo>
            <h3>Comments: </h3>

            <form onSubmit={handleSubmit}>
                <h4>
                    {submitType == "CREATE"
                        ? "Create Comment"
                        : "Update Comment"}
                </h4>
                <input
                    placeholder="content"
                    type="text"
                    name="content"
                    onChange={handleChange}
                    value={inputValues.content}
                />
                <button type="submit">send</button>
            </form>
            <Comments
                comments={comments}
                update={handleUpdate}
                postId={postId}
                deleteComment={deleteComment}
            />
        </PostPageContainer>
    );
};
