import SPHERE from "../assets/sphere.jpg";
import {
    InlineContainer,
    LogoImg,
    MainContainer,
    MainTitle,
} from "../styles/landingStyles";
import { ButtonLink } from "../ui/Button-link.ui";

export const Landing = () => {
    return (
        <>
            <MainContainer>
                <LogoImg src={SPHERE}></LogoImg>
                <MainTitle>The best blog in the world</MainTitle>

                <InlineContainer>
                    <ButtonLink color="green" path="/login" name="Login" />
                    <ButtonLink
                        color="orange"
                        path="/sign-up"
                        name="Sign up for free"
                    />
                </InlineContainer>
            </MainContainer>
        </>
    );
};
