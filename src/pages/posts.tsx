import { usePost } from "../hooks/usePost";
import { PostDTO } from "../interfaces/post.dto";
import { RedirectButtons } from "../styles/landingStyles";
import {
    ActionButton,
    AllPostsContainer,
    ButtonContainer,
    ErrorMessage,
    FormTitle,
    MainPostContainer,
    PostPageContainer,
    PostPageForm,
} from "../styles/postPageStyles";
import { Post } from "../ui/Post.ui";

export enum Type {
    UPDATE = "UPDATE",
    CREATE = "CREATE",
}

export const Posts = () => {
    const {
        posts,
        inputValues,
        submitType,
        errors,
        handleChange,
        handleSubmit,
        handleClear,
        handleUpdate,
        deletePost,
    } = usePost();

    return (
        <PostPageContainer>
            <PostPageForm onSubmit={handleSubmit}>
                <FormTitle>
                    {submitType == Type.CREATE ? "Create Post" : "Updating..."}
                </FormTitle>
                <input
                    type="text"
                    name="title"
                    placeholder="Type your title"
                    onChange={handleChange}
                    value={inputValues.title}
                />
                {errors.title && <ErrorMessage>{errors.title}</ErrorMessage>}
                <textarea
                    name="text"
                    placeholder="Type your text"
                    onChange={handleChange}
                    value={inputValues.text}
                />
                {errors.text && <ErrorMessage>{errors.text}</ErrorMessage>}

                <RedirectButtons color="green" type="submit">
                    Send
                </RedirectButtons>
                <RedirectButtons
                    color="orange"
                    type="button"
                    onClick={handleClear}
                >
                    Cancel
                </RedirectButtons>
            </PostPageForm>
            <AllPostsContainer>
                {posts?.allPost.map((post: PostDTO) => {
                    return (
                        <MainPostContainer key={post.id}>
                            <Post post={post} />
                            <ButtonContainer>
                                <ActionButton
                                    actionType="update"
                                    onClick={() => handleUpdate(post)}
                                >
                                    <i className="fa-solid fa-pen-to-square"></i>
                                </ActionButton>
                                <ActionButton
                                    onClick={() => deletePost(post.id)}
                                >
                                    <i className="fa-solid fa-ban"></i>
                                </ActionButton>
                            </ButtonContainer>
                        </MainPostContainer>
                    );
                })}
            </AllPostsContainer>
        </PostPageContainer>
    );
};
