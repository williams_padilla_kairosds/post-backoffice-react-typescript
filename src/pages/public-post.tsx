import { useViewPosts } from "../hooks/useViewPosts";
import { PostDTO } from "../interfaces/post.dto";
import {
    AllPostsContainer,
    MainPostContainer,
    PostPageContainer,
} from "../styles/postPageStyles";
import { Post } from "../ui/Post.ui";

export const PublicPosts = () => {
    const { posts } = useViewPosts();

    return (
        <PostPageContainer>
            <AllPostsContainer>
                {posts?.allPost.map((post: PostDTO) => {
                    return (
                        <MainPostContainer key={post.id}>
                            <Post post={post} />
                        </MainPostContainer>
                    );
                })}
            </AllPostsContainer>
        </PostPageContainer>
    );
};
