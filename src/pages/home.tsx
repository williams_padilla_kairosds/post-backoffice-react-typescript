import { useContext, useEffect, useState } from "react";
import { RoleContext } from "../store/role.context";
import { StyledLink } from "../styles/loginPageStyles";

export const Home = () => {
    const userContext = useContext(RoleContext);
    const [link, setLink] = useState(<></>);

    const redirect = () => {
        switch (userContext?.data.role) {
            case "ADMIN":
                setLink(<StyledLink to="/post">Edit posts</StyledLink>);
                break;
            case "AUTHOR":
                setLink(
                    <>
                        <StyledLink to="/post">My posts</StyledLink>
                        <StyledLink to="/public-posts">All post</StyledLink>
                    </>
                );
                break;
            case "USER":
                setLink(<StyledLink to="/public-posts">All post</StyledLink>);
                break;
        }
        return redirect;
    };

    const load = () => {
        localStorage.setItem("userData", JSON.stringify(userContext!.data));
    };

    useEffect(() => {
        window.addEventListener("beforeunload", load);
        redirect();
        return () => window.removeEventListener("beforeunload", load);
    }, []);

    return <div>{link}</div>;
};
