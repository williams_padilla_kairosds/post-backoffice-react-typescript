import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { NavBar } from "./components/navbar";
import "./index.css";
import { Router } from "./routes/router";
import initConfig from "./services/config/config.service";
import { init } from "./services/utils/axiosIntance";

initConfig.load().then(() => {
    init();
    ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
        <BrowserRouter basename={import.meta.env.BASE_URL}>
            <NavBar />
            <Router />
        </BrowserRouter>
    );
});
